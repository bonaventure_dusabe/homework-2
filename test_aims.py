from nose.tools import assert_almost_equal
import aims
import numpy as np
import math


def test_positive_numbers():
    numbers = [4, 5, 9]
    obs = aims.std(numbers)
    exp = 2.1602468994693
    assert_almost_equal(obs, exp)


def test_negative_numbers():
    numbers = [-2, -2, -4]
    obs = aims.std(numbers)
    exp =1.0
    assert_almost_equal(obs, exp)

def test_negative_numbers_float():
    numbers = [-1.5, -0.2, -0.4]
    obs = aims.std(numbers)
    exp =0.57154760664941
    assert_almost_equal(obs, exp)

def test_positive_numbers_float():
    numbers = [2.5, 3.5, 4.5, 10.5]
    obs = aims.std(numbers)
    exp =3.1124748994972
    assert_almost_equal(obs, exp)



def test_avg_range():
    files = ['data/bert/audioresult-00222']
    obs = aims.avg_range(files)
    exp =5
    assert_almost_equal(obs, exp)


def test_avg_range():
    files = ['data/bert/audioresult-00359']
    obs = aims.avg_range(files)
    exp =7
    assert_almost_equal(obs, exp)



def test_avg_range():
    files = ['data/bert/audioresult-00372']
    obs = aims.avg_range(files)
    exp =5
    assert_almost_equal(obs, exp)


def test_avg_range():
    files = ['data/bert/audioresult-00460']
    obs = aims.avg_range(files)
    exp =5
    assert_almost_equal(obs, exp)


def test_avg_range():
    files = ['data/bert/audioresult-00518']
    obs = aims.avg_range(files)
    exp =1
    assert_almost_equal(obs, exp)



