#! /usr/bin/env python
import numpy as np
import math

def main():
    print "Welcome to the AIMS module"

if __name__ == "__main__":
    main()

def mean(numbers):
    try:
        return sum(numbers) / len(numbers)
    except ZeroDivisionError:
        return 0

def std(lst):
    mn = mean(lst)
    variance = sum([(e-mn)**2.0 for e in lst])
    return math.sqrt(variance/float(len(lst)))


def avg_range(x):
    n=len(x)
    if n==0.0:
        return 0.0
    if type(x)!=list:
        return "Input is not a list."
    myfile=open(x[0],'r')
    for line in myfile:
        line=line.strip()
        if line.startswith('Range'):
            line_list=line.split(':')
            return int(line_list[1])



